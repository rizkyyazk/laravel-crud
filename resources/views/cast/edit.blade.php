@extends('layout.master')
@section('judul')
Update Data Cast
@endsection()
@section('content')
    <form action="/cast/{{$cast->id}}" method="POST">
        @csrf
        @method('put')
        <div class="form-group">
            <label for="">Nama</label>
            <input type="text" value="{{$cast->nama}}" name="nama" class="form-control" id="" placeholder="Masukkan nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="form-group">
            <label for="">Umur</label>
            <input type="text" value="{{$cast->umur}}" name="umur" class="form-control" id="" placeholder="Masukkan Umur">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="form-group">
            <label for="">Bio</label>
            <textarea class="form-control" name="bio" id="" cols="30" rows="5" placeholder="Masukkan Bio">{{$cast->bio}}</textarea>
        </div> 
        @error('bio')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection()