@extends('layout.master')
@section('judul')
Create Data Cast
@endsection()
@section('content')
    <form action="/cast" method="POST">
        @csrf
        <div class="form-group">
            <label for="">Nama</label>
            <input type="text" name="nama" class="form-control" id="" placeholder="Masukkan nama">
        </div>
        @error('nama')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="form-group">
            <label for="">Umur</label>
            <input type="text" name="umur" class="form-control" id="" placeholder="Masukkan Umur">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <div class="form-group">
            <label for="">Bio</label>
            <textarea class="form-control" name="bio" id="" cols="30" rows="5" placeholder="Masukkan Bio"></textarea>
        </div> 
        @error('bio')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection()